﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("********Alquiller de Barcos********");
            //IngresarBarcos();
            //ListaDeAlquileres();
            ModificarAlquiler();
        }
        static void IngresarBarcos()
        {
            using (var db = new EntityFrameworkContext())
            {
                Cliente cliente = new Cliente();
                Console.WriteLine("Ingrese su Nombre");
                cliente.Nombre = Console.ReadLine();
                Console.WriteLine("Ingrese su apellido");
                Console.WriteLine("Ingrese su cedula");
                db.Add(cliente);
                db.SaveChanges();

                Barco barco = new Barco();
                Console.WriteLine("Ingrese los datos de su barco");
                Console.WriteLine("Ingrese su Matricula");
                barco.NumMatricula = Console.ReadLine();
                Console.WriteLine("Ingrese su Largo en Metros");
                barco.Largo = Console.ReadLine();
                Console.WriteLine("Ingrese su año de fabricacion");

                db.Add(barco);
                db.SaveChanges();

                Alquiler alquiler = new Alquiler();
                Console.WriteLine("Datos de Posicionamiento entre otros");
                alquiler.Entrada= DateTime.Now;
                Console.WriteLine("Posicion de barco");

                ListaNumero();
                var posicion = (from resultado in db.posicions
                                where resultado.Id == Convert.ToInt32(Console.ReadLine())
                                select resultado).FirstOrDefault<Posicion>();
                alquiler.Posiscion = posicion.NumPosicion;
                List<Posicion> ListaEspacios = db.posicions.ToList();
                    
                


                db.Add(alquiler);
                db.SaveChanges();
            }
            return;
        }
        static void ModificarAlquiler()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Lista de Alquiler que desea Modificar");
                ListaDeAlquileres();
                Console.WriteLine("Ingrese digito a modificar");
                var alquiler = (from resultado in db.alquilers
                                where resultado.Id == Convert.ToInt32(Console.ReadLine())
                                select resultado).FirstOrDefault <Alquiler>();
                Console.WriteLine("Barco a modificar: " +alquiler.Barco);
                Console.WriteLine("Posicion a cambiar");
                alquiler.Posiscion = Console.ReadLine();
                db.Add(alquiler);
                db.SaveChanges();
            }

        }
        static void ListaDeAlquileres()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Alquiler> ListaAlquiler = db.alquilers.ToList();
                foreach (var alquiler in ListaAlquiler)
                {
                    Console.WriteLine("Fecha Entrada" + alquiler.Entrada+"Fecha de Salida"+alquiler.Final+"Posicion "+alquiler.Posiscion+"Tipo de Barco: "+alquiler.Barco);
                }
            }
            
        }
        static void ListadeRegistros()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Alquiler> ListaAlquiler = db.alquilers.ToList();
                foreach (var alquiler in ListaAlquiler)
                {
                    Console.WriteLine("Fecha Entrada" + alquiler.Entrada + "Fecha de Salida" + alquiler.Final + "Posicion " + alquiler.Posiscion + "Tipo de Barco: " + alquiler.Barco);
                }
            }
        }
        static void ListaNumero()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Posicion> ListaEspacios = db.posicions.ToList();
                foreach (var espacios in ListaEspacios)
                {
                    Console.WriteLine(espacios.Id + "Espacio Disponible: "+espacios.NumPosicion );
                }
            }

        }
    }
}
