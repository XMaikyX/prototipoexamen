﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class RegistroEmbarcaciones
    {
        public int Id { get; set; }
        public string NumMatricula { get; set; }
        public string Largo { get; set; }
        public string AnioFabricacion { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime Final { get; set; }
        public string Posiscion { get; set; }
        public string Barco { get; set; }
        public string Cliente { get; set; }

    }
}
