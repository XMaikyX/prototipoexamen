﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class EntityFrameworkContext : DbContext
    {
        private const string connectionString =
            "Server=.\\MICHAEL; Database=ConsoleBarcos; Trusted_Connection= True;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
        public DbSet<Cliente> clientes{ get; set; }
        public DbSet<Barco> barcos { get; set; }
        public DbSet<Alquiler> alquilers { get; set; }
        public DbSet<Deportivo> deportivos { get; set; }
        public DbSet<Velero> veleros { get; set; }
        public DbSet<Yate> yates { get; set; }
        public DbSet<Posicion> posicions { get; set; }  

    }
}
