﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Alquiler
    {
        double PrecioFijo = 1.80;
        public int Id { get; set; } 
        public DateTime Entrada { get; set; }
        public DateTime Final { get; set; }
        public string Posiscion { get; set; }
        public string Barco { get; set; }
        public int Precio { get; set; }

        
    }
}
