﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Barco
    {
        public int Id { get; set; }
        public string NumMatricula { get; set; }
        public string Largo { get; set; }
        public string AnioFabricacion { get; set; }
        public string Cliente { get; set; }

    }
}
